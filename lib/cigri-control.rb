require 'cigri-clusterlib'
require 'cigri-clientlib'
require 'cigri-joblib'
require 'json'

class Controller
  def initialize(logfile, cluster, config_file)
    while !File.file?(config_file)
      sleep 5
    end
    config_data = JSON.parse(File.read(config_file))
  
    @logfile = logfile # TODO: May need to create the file if does not exist
    @cluster = cluster
    @ref = config_data["ref"].nil? ? 0 : config_data["ref"].to_f
    @beta = config_data["beta"].nil? ? 0 : config_data["beta"].to_f
    @sensor = 0   

    @slope = 0
    @prev_sensor = 0

    case config_data["type"]
    when "pi"
      # @ctrlr = PIController.new(config_data)
      config_data["horizon_now"] = 0
      config_data["horizon_then"] = 0
      @ctrlr = PIFFController.new(config_data, cluster)
      @horizon_now = config_data["horizon_now"].to_i
      @horizon_then = config_data["horizon_then"].to_i
    when "pi_if"
      @ctrlr = PIControllerIf.new(config_data)
    when "step"
      @ctrlr = Steps.new(config_data)
    when "constant"
      @ctrlr = Constant.new(config_data, cluster)
    when "constant_horizon"
      @ctrlr = ConstantHorizon.new(config_data, cluster)
      @horizon_now = config_data["horizon_now"].to_i
      @horizon_then = config_data["horizon_then"].to_i
    when "pi_horizon"
      @ctrlr = PIFFController.new(config_data, cluster)
      @horizon_now = config_data["horizon_now"].to_i
      @horizon_then = config_data["horizon_then"].to_i
    when "pi_horizon2"
      @ctrlr = PIFFController2.new(config_data, cluster)
      @horizon_now = config_data["horizon_now"].to_i
      @horizon_then = config_data["horizon_then"].to_i
    else
      logger.warn("Controller type #{config_data["type"]} unknown")
    end
  end

  def update_controlled_value()
    @ctrlr.update_controlled_value()
  end

  def update_errors()
    error = @ctrlr.get_error()
    @ctrlr.update_errors(error)
  end

  def log()
    file = File.open(@logfile, "a+")
    file << "#{Time.now.to_i}, #{self.get_nb_jobs()}, #{self.get_waiting_jobs()}, #{self.get_running_jobs()}#{@ctrlr.log()}\n"
    file.close
  end

  def get_completion()
    completed_jobs = @cluster.get_completion()
    if completed_jobs["items"].length > 0
      completed_jobs["items"].map{|j|
          /R=([0-9]+),*/.match(j["message"]).captures()[0].to_i
      }.sum
    else
      0
    end
  end

  def get_nb_future_prio_resources(horizon_now, horizon_then)
    future_jobs = @cluster.get_gantt(horizon_now, horizon_then)
    if future_jobs["items"].length > 0
      future_jobs["items"].map{|j|
          /R=([0-9]+),*/.match(j["message"]).captures()[0].to_i
      }.sum
    else
      0
    end
  end

  def get_sensor()
    running = self.get_running_jobs()
    waiting = self.get_waiting_jobs()
    
    running + waiting
    # @sensor = @beta * @sensor  + (1 - @beta) * self.get_completion()
    # @sensor
  end

  def get_running_jobs()
    begin
      cluster_jobs = @cluster.get_jobs()
      nb_running_jobs = cluster_jobs.select{|j| j["queue_name"] == "besteffort" and (j["state"] == "Running" or j["state"] == "Launching" or j["state"] == "Finishing")}.map{|j| j["resources"].length}.sum
      # nb_running_jobs = cluster_jobs.select{|j| (j["state"] == "Running" or j["state"] == "Launching" or j["state"] == "Finishing")}.map{|j| j["resources"].length}.sum
      nb_running_jobs
    rescue
      0
    end
  end

  def get_waiting_jobs()
    begin
      cluster_jobs = @cluster.get_jobs()
      nb_waiting_jobs = cluster_jobs.select{|j| j["queue_name"] == "besteffort" and j["state"] == "Waiting"}.length
      nb_waiting_jobs
    rescue
      0
    end
  end

  def get_nb_jobs()
    bound_nb_jobs(@ctrlr.get_nb_jobs())
  end
end

class Steps
  def initialize(config)
    @step = -1
    @u_values = config["u_values"]
    @u = 0
  end

  def get_error()
    0
  end

  def update_errors(error)
    return
  end
  
  def update_controlled_value()
    @step = @step + 1
    if @step < @u_values.length
      @u = @u_values[@step].to_i
    end
  end
  
  def log()
    ""
  end
  
  def get_nb_jobs()
    @u
  end
end

class Constant
  def initialize(config, cluster)
    @rmax = config["rmax"].to_i
    @alpha = config["alpha"].to_f
    @u = @alpha * @rmax
    @horizon_now = config["horizon_now"].to_i
    @horizon_then = config["horizon_then"].to_i
    @cluster = cluster
  end

  def get_error()
    0
  end

  def update_errors(error)
    return
  end

  def get_average_duration()
    campaigns = @cluster.running_campaigns()
    if campaigns.length > 0
      running_campaign = campaigns[campaigns.length - 1]
      campaign = Cigri::Campaign.new(:id => running_campaign)
      # avg_std = running_campaign.average_job_duration()
      avg_std = campaign.average_job_duration
      avg = avg_std[0].to_f
      if avg > 0
        @alpha = 30.0 / avg
      end
    end

  end

  def get_nb_future_prio_resources(horizon_now, horizon_then)
    future_jobs = @cluster.get_gantt(horizon_now, horizon_then)
    if future_jobs["items"].length > 0
      future_jobs["items"].map{|j|
          /R=([0-9]+),*/.match(j["message"]).captures()[0].to_i
      }.sum
    else
      0
    end
  end
  
  def update_controlled_value()
    self.get_average_duration()
    @u = @alpha * @rmax
  end
  
  def log()
    ", #{self.get_nb_future_prio_resources(@horizon_now, @horizon_then)}, #{@alpha}"
  end
  
  def get_nb_jobs()
    @u
  end
end

class ConstantHorizon
  def initialize(config, cluster)
    @rmax = config["rmax"].to_i
    @alpha = config["alpha"].to_f
    @horizon_now = config["horizon_now"].to_i
    @horizon_then = config["horizon_then"].to_i
    @cluster = cluster
    @u = @alpha * @rmax
  end

  def get_error()
    0
  end

  def update_errors(error)
    return
  end

  def get_nb_future_prio_resources(horizon_now, horizon_then)
    future_jobs = @cluster.get_gantt(horizon_now, horizon_then)
    if future_jobs["items"].length > 0
      future_jobs["items"].map{|j|
          /R=([0-9]+),*/.match(j["message"]).captures()[0].to_i
      }.sum
    else
      0
    end
  end

  def get_average_duration()
    campaigns = @cluster.running_campaigns()
    if campaigns.length > 0
      running_campaign = campaigns[campaigns.length - 1]
      campaign = Cigri::Campaign.new(:id => running_campaign)
      # avg_std = running_campaign.average_job_duration()
      avg_std = campaign.average_job_duration
      avg = avg_std[0].to_f
      if avg > 0
        @alpha = 30.0 / avg
      end
    end

  end
  
  def update_controlled_value()
    future_prio = self.get_nb_future_prio_resources(@horizon_now, @horizon_then)
    self.get_average_duration()
    @u = @alpha * (@rmax - future_prio)
  end
  
  def log()
    ", #{self.get_nb_future_prio_resources(@horizon_now, @horizon_then)}, #{@alpha}"
  end
  
  def get_nb_jobs()
    @u
  end
end


class PIController
  def initialize(config)
    @u = 0
    @kp = config["kp"].to_f
    @ki = config["ki"].to_f
    @sum_error = 0.0
    @error = 0.0
  end

  def get_error()
    @ref - self.get_sensor()
  end

  def update_errors(error)
    @error = error
    @sum_error += error
  end
  
  def update_controlled_value()
    @u = @kp * @error + @ki * @sum_error
  end

  def log()
    ""
  end
  
  def get_nb_jobs()
    @u
  end
end

class PIFFController
  def initialize(config, cluster)
    @cluster = cluster
    @u = 0
    @kp = config["kp"].to_f
    @ki = config["ki"].to_f
    @rmax = config["rmax"].to_i
    @alpha = config["alpha"].to_f
    @horizon_now = config["horizon_now"].to_i
    @horizon_then = config["horizon_then"].to_i
    @sum_error = 0.0
    @error = 0.0
  end
  
  def get_average_duration()
    campaigns = @cluster.running_campaigns()
    if campaigns.length > 0
      running_campaign = campaigns[campaigns.length - 1]
      campaign = Cigri::Campaign.new(:id => running_campaign)
      # avg_std = running_campaign.average_job_duration()
      avg_std = campaign.average_job_duration
      avg = avg_std[0].to_f
      if avg > 0
        @alpha = 30.0 / avg
      end
    end

  end
  
  def get_nb_future_prio_resources(horizon_now, horizon_then)
    future_jobs = @cluster.get_gantt(horizon_now, horizon_then)
    if future_jobs["items"].length > 0
      future_jobs["items"].map{|j|
          /R=([0-9]+),*/.match(j["message"]).captures()[0].to_i
      }.sum
    else
      0
    end
  end

  def log()
    ", #{self.get_nb_future_prio_resources(@horizon_now, @horizon_then)}, #{@alpha}"
  end

  def get_error
    @ref - self.get_nb_future_prio_resources(@horizon_now, @horizon_then) - self.get_sensor()
  end

  def update_errors(error)
    @error = error
    @sum_error += error
  end
  
  def update_controlled_value()
    future_prio = self.get_nb_future_prio_resources(@horizon_now, @horizon_then)
    self.get_average_duration()
    # offset = @alpha * (@rmax + future_prio) # '+' because we counter affect the future disturbance
    offset = @alpha * @rmax  -  @alpha * future_prio
    @u = @kp * @error + @ki * @sum_error + offset
  end
  
  def get_nb_jobs()
    @u
  end
end

class PIFFController2
  def initialize(config, cluster)
    @cluster = cluster
    @u = 0
    @ks = config["ks"].to_f
    @mp = config["mp"].to_f
    @rmax = config["rmax"].to_i
    @alpha = config["alpha"].to_f
    @horizon_now = config["horizon_now"].to_i
    @horizon_then = config["horizon_then"].to_i
    @sum_error = 0.0
    @error = 0.0
    @r = Math.exp(-@ks/4)
    @theta = Math::PI * Math.log(@r) / Math.log(@mp)
    @ki = (1 - 2 * @r * Math.cos(@theta) + @r * @r) / 1
    @kp = (1 - @alpha -  @r * @r) / 1
  end

  def compute_gains()
    @kp = (1 - @alpha -  @r * @r) / 1
  end
  
  def get_average_duration()
    campaigns = @cluster.running_campaigns()
    if campaigns.length > 0
      running_campaign = campaigns[campaigns.length - 1]
      campaign = Cigri::Campaign.new(:id => running_campaign)
      # avg_std = running_campaign.average_job_duration()
      avg_std = campaign.average_job_duration
      avg = avg_std[0].to_f
      if avg > 0
        @alpha = 30.0 / avg
      end
    end

  end
  
  def get_nb_future_prio_resources(horizon_now, horizon_then)
    future_jobs = @cluster.get_gantt(horizon_now, horizon_then)
    if future_jobs["items"].length > 0
      future_jobs["items"].map{|j|
          /R=([0-9]+),*/.match(j["message"]).captures()[0].to_i
      }.sum
    else
      0
    end
  end

  def log()
    ", #{self.get_nb_future_prio_resources(@horizon_now, @horizon_then)}, #{@alpha}"
  end

  def get_error()
    @ref - self.get_nb_future_prio_resources(@horizon_now, @horizon_then) - self.get_sensor()
  end

  def update_errors(error)
    @error = error
    @sum_error += error
  end
  
  def update_controlled_value()
    # future_prio = self.get_nb_future_prio_resources(@horizon_now, @horizon_then)
    self.get_average_duration()
    self.compute_gains()
    # offset = @alpha * (@rmax + future_prio) # '+' because we counter affect the future disturbance
    offset = 0#  @alpha * future_prio
    @u = @kp * @error + @ki * @sum_error - offset
  end
  
  def get_nb_jobs()
    @u
  end
end

class PIControllerIf
  def initialize(config)
    @u = 0
    @gamma = config["gamma"].to_f
    @beta = config["beta"].to_f
    @kp1 = config["kp1"].to_f
    @kp2 = config["kp2"].to_f
    @ki = config["ki"].to_f
    @sum_error = 0.0
    @error = 0.0
  end

  def log()
    ""
  end

  def get_error()
    @ref - self.get_sensor()
  end

  def update_errors(error)
    @error = error
    @sum_error += error
  end
  
  def update_controlled_value()
    if @u < @gamma then
      @u = @kp1 * @error + @ki * @sum_error
    else
      @u = @kp2 * @error + @ki * @sum_error + @beta
    end
  end
  
  def get_nb_jobs()
    @u
  end
end

def p_controller(error, k)
  k * error
end

def pd_controller(error, previous_error, kp, kd)
  kp * error + kd  * (error - previous_error)
end

def min(x, y)
  if x < y
    x
  else
    y
  end
end

def bound_nb_jobs(nb_jobs)
  if nb_jobs < 0
    return 0
  end
  return nb_jobs
end
